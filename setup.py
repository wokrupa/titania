import setuptools.command.build_py

try:  # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <= 9.0.3
    from pip.req import parse_requirements

install_reqs = parse_requirements('requirements.txt', session=False)
reqs = [str(ir.requirement) for ir in install_reqs]

with open("README.md", "r") as fh:
    long_description = fh.read()
    fh.close()

setuptools.setup(
    # TODO surround imports with try catch and log when someone did not install root and want to use root packages
    extras_require={
        'root': ['root-pandas']
    },
    install_requires=reqs,
    name='titania',
    version='0.3',
    author="Maciej Majewski",
    author_email="mmajewsk@cern.ch",
    description="Titania monitoring framework",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mmajewsk/titania",
    package_dir={'titania': 'core'},
    packages=['titania'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ]

)
