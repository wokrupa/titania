from VELO.panels.velo_control_panel import ControlPanel
from core.QtGUI import SimpleTab
from core.plots import StackedAreaPlot


class StackedAreaTestTab(SimpleTab):
    def __init__(self, parent=None):
        self.parent = parent
        super().__init__(control_panel=ControlPanel)

    def set_plot(self):
        return StackedAreaPlot(parent=self.parent, widget=self)

    def set_title(self):
        return "StackedAreaPlotExample"
