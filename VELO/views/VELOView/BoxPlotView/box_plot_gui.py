from VELO.panels.velo_control_panel import ControlPanel
from core.QtGUI import SimpleTab
from core.data.examplary_generated_data import IrisData
from core.plots import BoxPlot


class BoxPlotTestTab(SimpleTab):
    def __init__(self, parent=None):
        self.parent = parent
        super().__init__(data=IrisData(), control_panel=ControlPanel)

    def set_plot(self):
        return BoxPlot(parent=self.parent, widget=self)

    def set_title(self):
        return "BoxPlotExample"
