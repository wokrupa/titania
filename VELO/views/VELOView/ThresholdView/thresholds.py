from core.common.titania_tab import TitaniaPlotTab
from core.data.examplary_generated_data import RandomNList
from core.plots import LinePlot


class Thresholds(TitaniaPlotTab):
    def __init__(self, plot_cls=LinePlot):
        TitaniaPlotTab.__init__(self, data=RandomNList())
        self.PlotClass = plot_cls

    def add_plots_to_layout(self):
        self.grid_layout.addLayout(self.control_panel.get_control_panel(), 0, 0)
        plot1 = self.PlotClass(parent=self.parent, widget=self).get_plot_widget(row=1)
        plot1.draw_plot(data=self.data)
        self.plot_panel_grid.addWidget(plot1, 0, 0, 1, 3)
        plot2 = self.PlotClass(parent=self.parent, widget=self).get_plot_widget(row=3)
        plot2.draw_plot(data=self.data)
        self.plot_panel_grid.addWidget(plot2, 2, 0, 1, 3)
        plot3 = self.PlotClass(parent=self.parent, widget=self).get_plot_widget(row=5)
        plot3.draw_plot(data=self.data)
        self.plot_panel_grid.addWidget(plot3, 4, 0, 1, 3)

    def set_plot(self):
        return LinePlot(parent=self)

    def set_title(self):
        return "ThresholdsTab"

    def create_control_panel(self):
        pass
