from core.plots import LinePlot
from .thresholds import Thresholds


class ThresholdsWeb(Thresholds):

    def set_plot(self):
        return LinePlot(parent=self)
