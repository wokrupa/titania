from VELO.panels.velo_control_panel import ControlPanel
from core.QtGUI import QtBaseLayoutTab
from core.data.examplary_generated_data import RandomNList
from core.plots import QtLinePlot
from .thresholds import Thresholds

class ThresholdsTab(QtBaseLayoutTab, Thresholds):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, data=RandomNList(), parent=parent)
        Thresholds.__init__(self, QtLinePlot)


    def add_plots_to_layout(self):
        Thresholds.add_plots_to_layout(self)

    def set_plot(self):
        return self.PlotClass(parent=self.parent, widget=self)

    def create_control_panel(self):
        return ControlPanel(widget=self)
