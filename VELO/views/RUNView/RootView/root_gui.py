from VELO.panels.velo_control_panel import ControlPanel
from core.QtGUI import SimpleTab

from core.data.root_data import RootData
from core.plots.root_hisogram_plot import RootScatterPlot


class RootPlot(SimpleTab):
    def __init__(self, parent=None):
        super().__init__(parent=parent, data=RootData(file_path='../resources/simple.root'), control_panel=ControlPanel)

    def set_plot(self):
        return RootScatterPlot(parent=self.parent, widget=self)

    def set_title(self):
        return "Root histogram"
