from VELO.data.matrix_data_conversion import MatrixDataConversion
from VELO.panels.velo_control_panel import ControlPanel
from VELO.plots.matrix_plot import MatrixPlot
from core.QtGUI import SimpleTab


class PedestalsPlot(SimpleTab):
    def __init__(self, parent=None):
        SimpleTab.__init__(self, parent=parent, data=MatrixDataConversion(), control_panel=ControlPanel)

    def set_plot(self):
        return MatrixPlot(parent=self.parent, widget=self )

    def set_title(self):
        return "PedestalsPlot"
