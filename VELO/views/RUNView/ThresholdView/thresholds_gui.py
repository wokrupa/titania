from VELO.panels.velo_control_panel import ControlPanel
from core.QtGUI import SimpleTab
from core.data.examplary_generated_data import RandomNList
from core.plots import HistogramPlot


class AnotherThresholdsPlot(SimpleTab):
    def __init__(self, parent=None):
        super().__init__(parent=parent, data=RandomNList(), control_panel=ControlPanel)

    def set_plot(self):
        return HistogramPlot(parent=self.parent, widget=self)

    def set_title(self):
        return "AnotherThresholdsPlot"
