from VELO.data.ut_data import UTMaskData
from VELO.panels.velo_control_panel import ControlPanel
from core.QtGUI import QtBaseLayoutTab
from core.plots.line_plot import QtUTMultiBarPlot2
from VELO.plots.matrix_plot import MatrixPlot
from .thresholds import Thresholds

class MaskView(QtBaseLayoutTab, Thresholds):
    def __init__(self, parent=None):
        self.parent = parent
        Thresholds.__init__(self, QtUTMultiBarPlot2)
        QtBaseLayoutTab.__init__(self, data=UTMaskData(), parent=parent)

    def add_plots_to_layout(self):
        Thresholds.add_plots_to_layout(self,1)

    def set_plot(self):
        return self.PlotClass(parent=self.parent, widget=self)

    def create_control_panel(self):
        return ControlPanel(widget=self)

    def set_title(self):
        return "Mask"


