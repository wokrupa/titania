from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

class UTPedestalsPlot(BaseCanvasPlot):
    def get_plot_widget(self):
        return self

    def pre_draw(self):
        self.figure.clear()