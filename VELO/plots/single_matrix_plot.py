from core.plots import NavToolbarPlot


class SingleMatrixPlot(NavToolbarPlot):
    def __init__(self, parent=None, widget=None, data=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)
        self.index = widget.index
        if data is not None and data.any():
            self.data_to_show = data
        else:
            self.data_to_show = self.widget.data.data[self.index]

    def draw_plot(self):
        self.figure.clear()
        self.ax = self.figure.add_subplot(111)
        self.im = self.ax.matshow(self.data_to_show, interpolation='nearest', cmap='YlOrRd')
        self.draw()