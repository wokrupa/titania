from VELO.views.RUNView.PedestalView.pedestals_gui import PedestalsPlot
from VELO.views.RUNView.PedestalsSinglePlotView.pedestals_single_plot_gui import PedestalSinglePlotWidget
from VELO.views.RUNView.RootView.root_gui import RootPlot
from VELO.views.RUNView.ScanFTrim.scan_FTrim_gui import ScanFTrimPlotWidget
from VELO.views.RUNView.ThresholdView.thresholds_gui import AnotherThresholdsPlot
from VELO.views.VELOView.AreaPlotTestTab.stacked_area_plot_gui import StackedAreaTestTab
from VELO.views.VELOView.BoxPlotView.box_plot_gui import BoxPlotTestTab
from VELO.views.VELOView.CountPlotTestTab.count_plot_gui import CountPlotTestTab
from VELO.views.VELOView.ScatterPlotTestTab.scatter_plot_gui import ScatterPlotTestTab
from VELO.views.VELOView.ScatterWithHistogramPlotTestTab.scatter_with_histogram_plot_gui import ScatterWithHistogramPlotTestTab
from VELO.views.VELOView.ThresholdView.thresholds_gui import ThresholdsTab
from VELO.views.UTView.Parameters.pedestals_gui import PedestalsView
from VELO.views.UTView.Parameters.noise_gui import NoiseView
from VELO.views.UTView.Parameters.mask_gui import MaskView

from VELO.views.UTView.Parameters.thresholds_gui import ThresholdsView

config = {
    # "VELOView": [ThresholdsTab, BoxPlotTestTab, StackedAreaTestTab, ScatterWithHistogramPlotTestTab, ScatterPlotTestTab, CountPlotTestTab],
    "VELOView": [ThresholdsTab, BoxPlotTestTab, StackedAreaTestTab, ScatterWithHistogramPlotTestTab, ScatterPlotTestTab],
    # "RUNView": [RootPlot, PedestalsPlot, AnotherThresholdsPlot, ScanFTrimPlotWidget, PedestalSinglePlotWidget]
    "RUNView": [PedestalsPlot, AnotherThresholdsPlot, ScanFTrimPlotWidget, PedestalSinglePlotWidget],
    "UTView": [PedestalsView, NoiseView, ThresholdsView, MaskView]

}
