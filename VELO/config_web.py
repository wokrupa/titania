#from VELO.views.RUNView.ThresholdView.thresholds_gui import AnotherThresholdsPlot
#from VELO.views.VELOView.AreaPlotTestTab.stacked_area_plot_gui import StackedAreaTestTab
#from VELO.views.VELOView.BoxPlotView.box_plot_gui import BoxPlotTestTab
#from VELO.views.VELOView.CountPlotTestTab.count_plot_gui import CountPlotTestTab
#from VELO.views.VELOView.ScatterPlotTestTab.scatter_plot_gui import ScatterPlotTestTab
#from VELO.views.VELOView.ScatterWithHistogramPlotTestTab.scatter_with_histogram_plot_gui import \
#    ScatterWithHistogramPlotTestTab

from VELO.views.VELOView.ThresholdView.thresholds_web import ThresholdsWeb

# config = {
#     "VELOView": [ThresholdsTab, BoxPlotTestTab, StackedAreaTestTab, ScatterWithHistogramPlotTestTab, ScatterPlotTestTab, CountPlotTestTab],
#     "RUNView": [AnotherThresholdsPlot, ]
# }
config = {
    "VELOView": [ThresholdsWeb],
}
