Click==7.0
cycler==0.10.0
kiwisolver==1.1.0
matplotlib==3.1.0
numpy==1.16.4
pyparsing==2.4.0
PyQt5==5.11.3
PyQt5-sip==4.19.15
python-dateutil==2.8.0
python-dotenv==0.10.1
six==1.12.0
seaborn
resources
jinja2
Flask==1.1.1
flask_nav
