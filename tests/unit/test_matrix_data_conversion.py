import unittest

from VELO.data.matrix_data_conversion import MatrixDataConversion


class MatrixDataConvertionTest(unittest.TestCase):

    def test_produced_matrix(self):
        matrix_data_conversion = MatrixDataConversion()

        self.assertEqual(256, len(matrix_data_conversion.data[0]))
        self.assertEqual(256, len(matrix_data_conversion.data[0][0]))

