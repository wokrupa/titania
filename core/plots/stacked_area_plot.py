# library & dataset
import matplotlib.pyplot as plt
from core.plots.base_plot import NavToolbarPlot


class StackedAreaPlot(NavToolbarPlot):

    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)


    def draw_plot(self, data=None):
        x = range(1, 6)
        y = [[1, 4, 6, 8, 9], [2, 2, 7, 10, 12], [2, 8, 5, 10, 6]]

        # Plot
        plt.stackplot(x, y, labels=['A', 'B', 'C'])
        plt.legend(loc='upper left')

        self.draw()

    def get_name(self):
        return "asd"

