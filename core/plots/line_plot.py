from core.plots.base_plot import NavToolbarPlot, MplPlot

class LinePlot(MplPlot):
    def __init__(self, parent=None, *args, **kwargs):
        MplPlot.__init__(self, parent=parent)

    def draw_plot(self, data=None):
        self.figure.clear()
        ax = self.figure.add_subplot(self.plot_number)
        ax.plot(self.parent.data.fetch(), '*-')
        self.draw()


class QtLinePlot(NavToolbarPlot):
    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self, data=None):
        self.figure.clear()
        ax = self.figure.add_subplot(self.plot_number)
        ax.plot(self.widget.data.fetch(), '*-')
        self.draw()

    def get_name(self):
        return "asd"


class QtUTMultiBarPlot(NavToolbarPlot):
    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self, data=None):
        data1, data2, data3 = self.widget.data.fetch()
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.3, wspace=0.3)

        ax = self.figure.add_subplot(212)
        ax.bar(range(len(data1)),data1, width=1.0, color='blue')
        ax.title.set_text((self.widget.data.get_title_list())[0])
        ax.set_xlabel((self.widget.data.get_x_label())[0])
        ax.set_ylabel(self.widget.data.get_y_label())        
        ax.set_xlim([0,len(data1)])    
        ax.grid()  
        self.draw()

        ax = self.figure.add_subplot(221)
        ax.plot(data2, '*', color='blue', markersize=9)
        ax.title.set_text((self.widget.data.get_title_list())[1])
        ax.set_xlabel((self.widget.data.get_x_label())[1])
        ax.set_ylabel(self.widget.data.get_y_label())
        ax.set_xticks([0,1,2,3])
        ax.grid()  
        self.draw()
    
        ax = self.figure.add_subplot(222)
        ax.plot(data3, '*', color='blue', markersize=9)
        ax.title.set_text((self.widget.data.get_title_list())[2])
        ax.set_xlabel((self.widget.data.get_x_label())[2])
        ax.set_ylabel(self.widget.data.get_y_label()) 
        ax.set_xticks([0])
        ax.grid()  
        self.draw()

class QtUTDoubleLinePlot(NavToolbarPlot):
    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self, data=None):
        data1, data2, data3 = self.widget.data.fetch()
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.4, wspace=0.4)

        ax1, ax2 = self.figure.subplots(2)

        ax1.plot(data1, '_', color='blue', markersize=7)
        ax1.title.set_text((self.widget.data.get_title_list())[0])
        ax1.set_xlabel((self.widget.data.get_x_label())[0])
        ax1.set_ylabel(self.widget.data.get_y_label())        
        ax1.set_xlim([0,len(data2)])    
        ax1.grid()  
        self.draw()

        ax2.plot(data2, '_', color='blue', markersize=7)
        ax2.title.set_text((self.widget.data.get_title_list())[1])
        ax2.set_xlabel((self.widget.data.get_x_label())[1])
        ax2.set_ylabel(self.widget.data.get_y_label())        
        ax2.set_xlim([0,len(data2)])    
        ax2.grid()  
        self.draw()

class QtUTSingleBarPlo2(NavToolbarPlot):
    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self, data=None):
        data = self.widget.data.fetch()
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.3, wspace=0.3)

        ax = self.figure.add_subplot(1)
        ax.bar(range(len(data)),data, width=1.0, color='blue')
        ax.title.set_text((self.widget.data.get_title_list())[0])
        ax.set_xlabel((self.widget.data.get_x_label())[0])
        ax.set_ylabel(self.widget.data.get_y_label())        
        ax.set_xlim([0,len(data)])    
        ax.grid()  
        self.draw()

class QtUTMultiBarPlot2(NavToolbarPlot):
    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self, data=None):
        data_channels = self.widget.data.fetch()
        data_asic = []
        for i in range(4):
            data_asic.append((data_channels [i*128:(i+1)*128-1]).count(1))
        data_sensor = data_channels .count(1)
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.3, wspace=0.3)

        ax = self.figure.add_subplot(212)
        ax.bar(range(len(data_channels)),data_channels, width=1.0, color='blue')
        ax.title.set_text((self.widget.data.get_title_list())[0])
        ax.set_xlabel((self.widget.data.get_x_label())[0])
        ax.set_ylabel((self.widget.data.get_y_label())[0])       
        ax.set_xlim([0,len(data_channels)])    
        ax.grid()  
        self.draw()

        ax = self.figure.add_subplot(221)
        ax.plot(data_asic, '*', color='blue', markersize=9)
        ax.title.set_text((self.widget.data.get_title_list())[1])
        ax.set_xlabel((self.widget.data.get_x_label())[1])
        ax.set_ylabel((self.widget.data.get_y_label())[1])
        ax.set_xticks([0,1,2,3])
        ax.grid()  
        self.draw()
    
        ax = self.figure.add_subplot(222)
        ax.plot(data_sensor, '*', color='blue', markersize=9)
        ax.title.set_text((self.widget.data.get_title_list())[2])
        ax.set_xlabel((self.widget.data.get_x_label())[2])
        ax.set_ylabel((self.widget.data.get_y_label())[2])
        ax.set_xticks([0])
        ax.grid()  
        self.draw()


# fun or loop

    def get_name(self):
        return "asd"

        