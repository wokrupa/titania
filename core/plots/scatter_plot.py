import seaborn as sns
from core.plots.base_plot import NavToolbarPlot


class ScatterPlot(NavToolbarPlot):

    def __init__(self, parent=None, widget=None):
        NavToolbarPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self):
        data = self.widget.data.fetch()
        sns.regplot(x=data["sepal_length"], y=data["sepal_width"])
        self.draw()

