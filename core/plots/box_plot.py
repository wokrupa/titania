# library & dataset
import seaborn as sns
from core.plots.base_plot import BaseCanvasPlot


class BoxPlot(BaseCanvasPlot):

    def __init__(self, parent=None, widget=None):
        BaseCanvasPlot.__init__(self, parent=parent, widget=widget)

    def draw_plot(self):
        data = self.widget.data.fetch()
        sns.boxplot(x=data["species"], y=data["sepal_length"], linewidth=5)
        self.draw()

    def get_name(self):
        return "asd"


